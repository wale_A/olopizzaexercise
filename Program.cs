﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace OloExercise
{
    class Program
    {
        static void Main(string[] args)
        {

            IEnumerable<PizzaToppings> toppingRequests = getCustomerToppingsRequest();
            Dictionary<string, int> toppingRank = createToppingsWithFrequencyOfOccurrence(toppingRequests);
            printToppingsWithRank(toppingRank);

            // pause, so console doesn't close
            Console.ReadLine();
        }

        private static Dictionary<string, int> createToppingsWithFrequencyOfOccurrence(IEnumerable<PizzaToppings> toppingRequests)
        {
            Dictionary<string, int> toppingRank = new Dictionary<string, int>();
            foreach (var pizzaTopping in toppingRequests)
            {
                // toppings might be repeated e.g "chicken, shrimp" is the same as "shrimp, chicken"
                var re_arrangedToppings = pizzaTopping.Toppings.OrderBy(x => x);
                // join string together with a comma to form one word
                var topping = string.Join(", ", re_arrangedToppings);
                if (toppingRank.ContainsKey(topping))
                {
                    toppingRank[topping] += 1;
                }
                else
                {
                    toppingRank.Add(topping, 1);
                }
            }

            return toppingRank;
        }

        private static void printToppingsWithRank(Dictionary<string, int> toppingRank)
        {
            // order toppings from most occurring to least occurring
            var orderedToppings = toppingRank.OrderByDescending(x => x.Value);
            Console.WriteLine("Index    --     Topping     --    Frequency");
            for (var i = 0; i < toppingRank.Count; i++)
            {
                var currentTopping = orderedToppings.ElementAtOrDefault(i);
                Console.WriteLine(string.Format("{0} | {1} | {2}", i + 1, currentTopping.Key, currentTopping.Value));
            }
        }

        private static IEnumerable<PizzaToppings> getCustomerToppingsRequest()
        {
            using (var client = new HttpClient())
            {
                var url = "http://files.olo.com/pizzas.json";
                var stringResponse = client.GetStringAsync(url).Result;
                IEnumerable<PizzaToppings> requests = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PizzaToppings>>(stringResponse);
                return requests;
            }
        }
    }
}
